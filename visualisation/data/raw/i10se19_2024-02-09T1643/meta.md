Measurement Start: `2024-02-09T164304+01:00` (epoch: `1707493384`)
Measurement End  : `2024-02-09T164340+01:00` (epoch: `1707493420`)
Host: `i10se19`
Kernel: `Linux i10se19 5.14.21-150500.55.44-default #1 SMP PREEMPT_DYNAMIC Mon Jan 15 10:03:40 UTC 2024 (cc7d8b6) aarch64 aarch64 aarch64 GNU/Linux`
Perf Version: `perf version 5.14.21`
All Args: `Cli { nodeid_length: 9, mesh_x: 8, mesh_y: 8, cores_per_dsu: 2, events: Some(["mxp_n_dat_txflit_valid", "mxp_e_dat_txflit_valid", "mxp_s_dat_txflit_valid", "mxp_w_dat_txflit_valid", "mxp_p0_dat_txflit_valid", "mxp_p1_dat_txflit_valid"]), outdir: "data", command: Launch(LaunchArgs { core_map: None, shell: None, env: None, pwd: None, binary: "/u/home/friese/build/core-to-core-latency/target/release/core-to-core-latency", args: Some(["-c", "60,42", "50000", "5000"]) }) }`
Additional Args: ``
